import numpy as np
import pandas as pd

from Network import Network
from activation_functions import sigmoid
import Layer
from metrics import MSE, R2, RMSE
from plot import plot_loss
from tools import default_init
from sklearn.model_selection import train_test_split


def get_predicted_data(input_data, network: Network):
    predict_dat = []
    for item in input_data:
        predict_dat.append(network.forward_prop(item)[0])
    predict_dat = np.array(predict_dat)
    return predict_dat


def full_train(target_name, df: pd.DataFrame):
    df_cleared = df.dropna()
    df_cleared = ((df_cleared - df_cleared.min()) / (df_cleared.max() - df_cleared.min()))

    all_attributes = df.columns.to_list()
    output_attributes = [target_name]
    input_attributes = np.setdiff1d(np.array(all_attributes), output_attributes)

    input_data = df_cleared[input_attributes].to_numpy()
    output_data = df_cleared[output_attributes].to_numpy()

    train_in_data, test_in_data, train_out_data, test_out_data = train_test_split(input_data, output_data,
                                                                                  test_size=0.3, shuffle=False)

    net = Network()
    net.add_layer(Layer.SimpleLayer(90, sigmoid))
    net.add_layer(Layer.SimpleLayer(1, sigmoid))
    net.init_net(init_strategy=default_init, input_shape=(input_data.shape[1],))
    net.train(input_train_data=train_in_data, target_train_data=train_out_data, learning_rate=0.0001, epoch_count=100,
              loss=MSE)

    plot_loss(net.loss_list)

    test_predicted_data = get_predicted_data(input_data=test_in_data, network=net)
    test_out_data = np.array([item[0] for item in test_out_data])

    print('______________' + target_name + '______________')
    y_avg = np.average(train_out_data)
    print('MSE:', MSE(test_predicted_data, test_out_data))
    print('RMSE:', RMSE(test_predicted_data, test_out_data))
    print('R2:', R2(test_predicted_data, test_out_data, y_avg))
    print()


# df_kgf = pd.read_csv('data_sets/NewList2.csv')
# df_kgf.drop('G_total (кг/с)', axis=1, inplace=True)
df_kgf = pd.read_csv('data_sets/KGF_List.csv')
full_train(target_name='КГФ (г/м3)', df=df_kgf)
# df_g_total = pd.read_csv('data_sets/NewList2.csv')
# df_g_total.drop('КГФ (г/м3)', axis=1, inplace=True)
df_g_total = pd.read_csv('data_sets/G_total_List.csv')
full_train(target_name='G_total (кг/с)', df=df_g_total)
