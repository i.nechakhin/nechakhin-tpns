import numpy as np
import plotly.express as px
import matplotlib.pyplot as plt


def plot_loss(loss_list):
    x = np.arange(1, len(loss_list) + 1)
    y = np.array(loss_list)
    fig = px.line(x=x, y=y, labels={'x': 'epoch', 'y': 'loss'})
    fig.show()


def plot_roc_curve(fpr, tpr):
    plt.plot(fpr, tpr, color='red', label='ROC')
    plt.plot([0, 1], [0, 1], color='green', linestyle='--')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic Curve')
    plt.legend()
    plt.show()
