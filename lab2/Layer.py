from abc import ABC, abstractmethod

from activation_functions import *
import numpy as np


class Layer(ABC):
    @abstractmethod
    def forward(self, input_vector):
        pass

    @abstractmethod
    def backward(self, input_data):
        pass

    @abstractmethod
    def init_layer(self, init_strategy):
        pass

    @abstractmethod
    def set_input_shape(self, input_shape):
        pass

    @abstractmethod
    def update_parameters(self, *update_args):
        pass


class SimpleLayer(Layer):
    def __init__(self, neuron_count, act_function):
        self.neuron_count = neuron_count
        self.input_shape = None
        self.act_function = act_function
        self.input_vector = None
        self.activations = None
        self.weight_matrix = None
        self.biases = None
        self.weights_grad_matrix = None
        self.biases_grad_array = None

    def set_weight_matrix(self, matrix):
        self.weight_matrix = matrix

    def forward(self, input_vector):
        self.input_vector = input_vector
        self.activations = np.dot(self.weight_matrix, self.input_vector) + self.biases
        return np.apply_along_axis(func1d=self.act_function, axis=0, arr=self.activations)

    def update_parameters(self, update_coefficient):
        l = lambda x: x * update_coefficient
        self.weight_matrix -= np.apply_along_axis(func1d=l, axis=0, arr=self.weights_grad_matrix)
        self.biases -= np.apply_along_axis(func1d=l, axis=0, arr=self.biases_grad_array)

    def backward(self, z_grad_array):
        diag_der = np.diag(np.apply_along_axis(func1d=get_der(self.act_function), axis=0, arr=self.activations))
        act_grad_array = np.dot(diag_der, z_grad_array)

        self.weights_grad_matrix = np.dot(np.array([act_grad_array]).transpose(), np.array([self.input_vector]))
        self.biases_grad_array = act_grad_array

        return np.dot(self.weight_matrix.transpose(), self.biases_grad_array)

    def set_input_shape(self, shape):
        self.input_shape = shape

    def init_layer(self, init_strategy):
        weight_matrix_shape = (self.neuron_count, self.input_shape[0])
        biases_shape = (self.neuron_count,)
        self.weight_matrix = init_strategy(weight_matrix_shape)
        self.biases = init_strategy(biases_shape)
        self.weights_grad_matrix = np.zeros(weight_matrix_shape)
        self.biases_grad_array = np.zeros(biases_shape)


class SoftmaxLayer(Layer):
    def __init__(self, neuron_count):
        self.neuron_count = neuron_count
        self.output_vector = None

    def forward(self, input_vector):
        exp_arr = np.exp(input_vector)
        self.output_vector = exp_arr / np.sum(exp_arr)
        return self.output_vector

    def backward(self, z_grad_array):
        return z_grad_array

    def init_layer(self, init_strategy):
        pass

    def set_input_shape(self, input_shape):
        pass

    def update_parameters(self, *update_args):
        pass
