from typing import Tuple, Any

import numpy as np
import pandas
import pandas as pd
import plotly.express as px
import plotly.figure_factory as ff

from sklearn.preprocessing import LabelEncoder


def read_from_file(file_name, sep: str):
    columns = ["edibility", "cap-shape", "cap-surface", "cap-color", "bruises", "odor",
               "gill-attachment", "gill-spacing", "gill-size", "gill-color",
               "stalk-shape", "stalk-root", "stalk-surface-above-ring",
               "stalk-surface-below-ring", "stalk-color-above-ring",
               "stalk-color-below-ring", "veil-type", "veil-color", "ring-number",
               "ring-type", "spore-print-color", "population", "habitat"]
    f = open(file_name, 'r')
    lines = f.readlines()
    data_list = []
    idx = 0
    for line in lines:
        dataset_line = np.array(line.replace("\n", "").split(sep))
        data_list.append(dataset_line)
        idx += 1
    return pd.DataFrame(data=data_list, index=np.arange(0, len(lines), 1), columns=columns)


def convert_all_values(df: pandas.DataFrame, target_name):
    input_attributes = np.setdiff1d(np.array(df.columns), target_name)
    output_attributes = target_name
    df = df.apply(LabelEncoder().fit_transform)
    return df[input_attributes], df[output_attributes]


def minmax_in_column(df: pandas.DataFrame, column: '') -> Tuple[Any, Any]:
    min_value = df[column].min()
    max_value = df[column].max()
    return min_value, max_value


def split_column_according_sturges_rule(df: pandas.DataFrame, col: '') -> []:
    n = 1 + round(np.log2(len(df.index) - df['edibility'].isna().sum()))
    min, max = minmax_in_column(df, col)
    a = (max - min) / n
    res = []
    for i in range(1, n + 1):
        res.append([min + a * (i - 1), min + a * i, 0])
    for i in range(0, len(df.index)):
        for j in range(0, n):
            if df.at[i, col] is not None and res[j][0] <= df.at[i, col] <= res[j][1]:
                res[j][2] += 1
                break
    return res


def entropy(freq: []) -> float:
    N = sum(freq)
    if N == 0:
        return 0
    final_sum = 0
    for i in range(0, len(freq)):
        pi = freq[i] / N
        if pi != 0:
            final_sum -= pi * np.log2(pi)
    return final_sum


def conditional_entropy(df: pandas.DataFrame, target: '', feature: '', target_splitting: [[]]):
    res = split_column_according_sturges_rule(df, feature)
    N = sum([res[j][2] for j in range(0, len(res))])
    final_sum = 0
    for j in range(0, len(res)):
        freq = [0 for _ in range(len(res))]
        for i in range(0, len(df.index)):
            if df.at[i, feature] is not None and res[j][0] <= df.at[i, feature] <= res[j][1]:
                for k in range(0, len(target_splitting)):
                    if df.at[i, target] is not None and target_splitting[k][0] <= df.at[i, target] <= \
                            target_splitting[k][1]:
                        freq[k] += 1
        final_sum += entropy(freq) * (res[j][2] / N)
    return final_sum


def split_info(df: pandas.DataFrame, feature: ''):
    res = split_column_according_sturges_rule(df, feature)
    N = sum([res[j][2] for j in range(0, len(res))])
    final_sum = 0
    for j in range(0, len(res)):
        ni = res[j][2] / N
        if ni != 0:
            final_sum -= ni * np.log2(ni)
    return final_sum


def all_gain_ratio_for_target(df: pandas.DataFrame, target: '') -> {}:
    split_target = split_column_according_sturges_rule(df, target)
    freq = []
    for i in range(0, len(split_target)):
        freq.append(split_target[i][2])

    res_dict = dict()
    for temp_col in df.columns:
        if temp_col != target:
            res_dict[temp_col] = \
                (entropy(freq) - conditional_entropy(df, target, temp_col, split_target)) / split_info(df, temp_col)
    return res_dict


def build_all_gain_ratios_for_target(df: pandas.DataFrame):
    gain_ratio_edibility = all_gain_ratio_for_target(df, "edibility")
    fig = px.bar(y=gain_ratio_edibility.keys(), x=gain_ratio_edibility.values(), title="edibility gain ratios")
    fig.update_layout(bargap=0)
    fig.show()


def build_corr_matrix(df: pandas.DataFrame):
    df_corr = df.corr(numeric_only=False).round(decimals=2)

    fig = ff.create_annotated_heatmap(
        np.array(abs(df_corr)),
        x=list(df_corr.columns),
        y=list(df_corr.index),
        annotation_text=np.around(np.array(abs(df_corr)), decimals=2),
        hoverinfo='z',
        colorscale='Viridis'
    )
    fig.update_layout(width=1400, height=1000)
    fig.show()


def check_all_cross_corr(df: pandas.DataFrame):
    df_corr = df.corr(numeric_only=False).round(decimals=2)
    gain_ratio_edibility = all_gain_ratio_for_target(df, 'edibility')
    for col1 in df_corr.columns:
        for col2 in df_corr.columns:
            sum = 0
            for i in df_corr.columns:
                sum += abs(df_corr.at[i, col1] - df_corr.at[i, col2])
            if sum < 3 and col1 != col2:
                print(col1, col2, sum)
                print("IGR edibility: ", gain_ratio_edibility[col1], gain_ratio_edibility[col2])
                print()


def main():
    results = read_from_file('data_sets/agaricus-lepiota.data', sep=",")
    results = results.apply(LabelEncoder().fit_transform)

    # remove due to power = 1
    results.drop("veil-type", axis=1, inplace=True)

    # build_all_gain_ratios_for_target(results)
    build_corr_matrix(results)
    # check_all_cross_corr(results)

    # remove due to low correlation and gain ratio
    results.drop("cap-shape", axis=1, inplace=True)
    results.drop("cap-surface", axis=1, inplace=True)
    results.drop("cap-color", axis=1, inplace=True)

    # remove due to has same feature
    results.drop("gill-attachment", axis=1, inplace=True)
    results.drop("gill-color", axis=1, inplace=True)
    results.drop("stalk-surface-below-ring", axis=1, inplace=True)
    results.drop("stalk-color-below-ring", axis=1, inplace=True)

    # remove due to low correlation
    results.drop("veil-color", axis=1, inplace=True)
    results.drop("stalk-surface-above-ring", axis=1, inplace=True)
    results.drop("habitat", axis=1, inplace=True)

    build_corr_matrix(results)
    build_all_gain_ratios_for_target(results)

    results.to_csv('Mush2.csv', index=False)


if __name__ == "__main__":
    main()
