import numpy as np
import pandas as pd

from Network import Network
from activation_functions import sigmoid
import Layer
from metrics import MSE, accuracy, recall, precision, false_positive_rate, true_positive_rate
from plot import plot_loss, plot_roc_curve
from tools import default_init
from sklearn.model_selection import train_test_split


def get_predicted_data(input_data, network: Network):
    predict_dat = []
    for item in input_data:
        predict_dat.append(network.forward_prop(item))
    predict_dat = np.array(predict_dat)
    return predict_dat


def full_train(target_name, df: pd.DataFrame):
    all_attributes = df.columns.to_list()
    output_attributes = [target_name]
    input_attributes = np.setdiff1d(np.array(all_attributes), output_attributes)

    input_data = np.array(df[input_attributes])
    output_data = np.array(df[output_attributes])

    train_in_data, test_in_data, train_out_data, test_out_data = train_test_split(input_data, output_data,
                                                                                  test_size=0.3, shuffle=False)

    net = Network()
    net.add_layer(Layer.SimpleLayer(100, sigmoid))
    # net.add_layer(Layer.SimpleLayer(30, sigmoid))
    net.add_layer(Layer.SimpleLayer(1, sigmoid))
    net.init_net(init_strategy=default_init, input_shape=(input_data.shape[1],))
    net.train(input_train_data=train_in_data, target_train_data=train_out_data, learning_rate=0.001, epoch_count=15,
              loss=MSE)

    plot_loss(net.loss_list)

    test_predicted_data = get_predicted_data(input_data=test_in_data, network=net)

    positive_label = np.array([0])  # edible
    negative_label = np.array([1])  # poisonous

    for i in range(len(test_predicted_data)):
        if test_predicted_data[i][0] > 0.5:
            test_predicted_data[i][0] = 1
        else:
            test_predicted_data[i][0] = 0

    print("Test metrics:")
    print("Accuracy - " + str(accuracy(positive_label, negative_label, test_predicted_data, test_out_data)))
    test_recall = recall(positive_label, test_predicted_data, test_out_data)
    print("Recall - " + str(test_recall))
    test_precision = precision(positive_label, negative_label, test_predicted_data, test_out_data)
    print("Precision - " + str(test_precision))
    print("F1-measure - " + str(2 * test_precision * test_recall / (test_precision + test_recall)))

    arr_false_positive_rate = []
    arr_true_positive_rate = []
    for p in range(0, 21):
        test_predicted_data = get_predicted_data(input_data=test_in_data, network=net)
        for i in range(len(test_predicted_data)):
            if test_predicted_data[i][0] > (p * 0.05):
                test_predicted_data[i][0] = 1
            else:
                test_predicted_data[i][0] = 0
        arr_false_positive_rate.append(false_positive_rate(negative_label, test_predicted_data, test_out_data))
        arr_true_positive_rate.append(true_positive_rate(positive_label, test_predicted_data, test_out_data))

    plot_roc_curve(arr_false_positive_rate, arr_true_positive_rate)


df_edibility = pd.read_csv('data_sets/Mush2.csv')
full_train(target_name='edibility', df=df_edibility)
