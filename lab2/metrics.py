import math

import numpy as np


def MSE(predicted_vector, target_vector) -> float:
    return np.sum(((target_vector - predicted_vector) ** 2)) / len(target_vector)


def RMSE(predicted_vector, target_vector) -> float:
    return math.sqrt(MSE(predicted_vector, target_vector))


def R2(predicted_vector, target_vector, avg) -> float:
    return 1 - (np.sum((target_vector - predicted_vector) ** 2) / np.sum((target_vector - avg) ** 2))


def log_loss(predicted_vector, dst_vector) -> float:
    return -np.sum(np.log(predicted_vector) * dst_vector)


def calc_outcome(label, predicted_data, actual_data):
    outcome = 0
    for i in range(len(actual_data)):
        actual_item = actual_data[i]
        predicted_item = predicted_data[i]
        if np.array_equal(label, actual_item) and np.array_equal(label, predicted_item):
            outcome += 1
    return outcome


def calc_actual_class_count(label, actual_data):
    actual_count = 0
    for i in range(len(actual_data)):
        actual_item = actual_data[i]
        if np.array_equal(label, actual_item):
            actual_count += 1
    return actual_count


def recall(positive_label, predicted_data, actual_data):
    actual_positive_count = calc_actual_class_count(positive_label, actual_data)
    true_positive = calc_outcome(positive_label, predicted_data, actual_data)
    return true_positive / actual_positive_count


def accuracy(positive_label, negative_label, predicted_data, actual_data):
    true_positive = calc_outcome(positive_label, predicted_data, actual_data)
    true_negative = calc_outcome(negative_label, predicted_data, actual_data)
    return (true_positive + true_negative) / predicted_data.shape[0]


def precision(positive_label, negative_label, predicted_data, actual_data):
    actual_negative_count = calc_actual_class_count(negative_label, actual_data)
    true_positive = calc_outcome(positive_label, predicted_data, actual_data)
    true_negative = calc_outcome(negative_label, predicted_data, actual_data)
    if (true_positive + (actual_negative_count - true_negative)) == 0:
        return None
    return true_positive / (true_positive + (actual_negative_count - true_negative))


def true_positive_rate(positive_label, predicted_data, actual_data):
    return recall(positive_label, predicted_data, actual_data)


def false_positive_rate(negative_label, predicted_data, actual_data):
    actual_negative_count = calc_actual_class_count(negative_label, actual_data)
    true_negative = calc_outcome(negative_label, predicted_data, actual_data)
    false_positive = actual_negative_count - true_negative
    return false_positive / (false_positive + true_negative)
