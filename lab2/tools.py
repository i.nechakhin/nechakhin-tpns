import numpy as np


def uniform_init(interval: tuple, array_shape: tuple):
    max_val = interval[1]
    min_val = interval[0]
    delta = max_val - min_val
    return min_val + np.random.rand(*array_shape) * delta


def default_init(array_shape: tuple):
    return uniform_init((-0.5, 0.5), array_shape)
