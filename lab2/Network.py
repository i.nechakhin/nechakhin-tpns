import Layer
import numpy as np


class Network:
    def __init__(self):
        self.layers = []
        self.layers_count = 0
        self.loss_function = None
        self.loss_list = []

    def add_layer(self, layer: Layer):
        self.layers.append(layer)
        self.layers_count += 1

    def forward_prop(self, input_vector):
        in_v = input_vector
        for layer in self.layers:
            in_v = layer.forward(in_v)
        return in_v

    def train_step(self, input_vector, target_vector, learning_rate: float):
        predicted = self.forward_prop(input_vector)
        loss_value = self.loss_function(predicted, target_vector)

        # back propagation
        last_index = self.layers_count - 1
        z_grad_array = None
        for i in range(last_index, -1, -1):
            cur_layer = self.layers[i]
            if i == last_index:
                z_grad_array = cur_layer.backward((2 / len(target_vector)) * (predicted - target_vector))
            else:
                z_grad_array = cur_layer.backward(z_grad_array)
            cur_layer.update_parameters(learning_rate)

        return loss_value

    def train(self, input_train_data, target_train_data, learning_rate: float, epoch_count: int, loss):
        samples_count = len(input_train_data)
        self.loss_function = loss

        for epoch in range(epoch_count):
            epoch_losses = []
            for i in range(samples_count):
                sample = input_train_data[i]
                target_sample = target_train_data[i]
                sample_loss = self.train_step(sample, target_sample, learning_rate)
                epoch_losses.append(sample_loss)
            cost = np.average(np.array(epoch_losses))
            # print("loss = " + str(cost))
            self.loss_list.append(cost)

    def test(self, input_test_data, target_test_data, loss) -> float:
        samples_count = len(input_test_data)
        losses = []
        for i in range(samples_count):
            sample = input_test_data[i]
            target_sample = target_test_data[i]
            output = self.forward_prop(sample)
            sample_loss = loss(output, target_sample)
            losses.append(sample_loss)

        return np.average(np.array(losses))

    def init_net(self, init_strategy, input_shape):
        for i in range(len(self.layers)):
            cur_layer = self.layers[i]
            if i == 0:
                cur_layer.set_input_shape(input_shape)
            else:
                prev_layer = self.layers[i - 1]
                cur_input_shape = (prev_layer.neuron_count,)
                cur_layer.set_input_shape(cur_input_shape)
            cur_layer.init_layer(init_strategy)
