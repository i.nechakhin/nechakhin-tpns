from typing import Tuple, Any

import numpy
import numpy as np
import pandas
import pandas as pd
import matplotlib.pyplot as plt
import plotly.express as px
import plotly.figure_factory as ff
import seaborn

from collections import Counter


def convert_all_values(df: pandas.DataFrame) -> pandas.DataFrame:
    for col in df.columns:
        for i in range(0, len(df.index)):
            if type(df.at[i, col]) == str:
                if "," in df.at[i, col]:
                    df.at[i, col] = float(df.at[i, col].replace(',', '.'))
                else:
                    df.at[i, col] = None
    return df


def remove_row_with_many_gaps(df: pandas.DataFrame) -> pandas.DataFrame:
    all_value = len(df.columns)
    for i in range(0, len(df.index)):
        none_value = df.iloc[i].isna().sum()
        if (none_value / all_value) * 100 > 60:
            df = df.drop(index=[i])
    df.index = np.arange(len(df))
    return df


def remove_column_with_many_gaps(df: pandas.DataFrame) -> pandas.DataFrame:
    all_value = len(df.index)
    for col in df.columns:
        none_value = df[col].isna().sum()
        if (none_value / all_value) * 100 > 60 and col != "G_total (кг/с)" and col != "КГФ (г/м3)":
            df.drop(col, axis=1, inplace=True)
    return df


def my_mode(sample):
    count = Counter(sample)
    return [key for key, value in count.items() if value == count.most_common(1)[0][1]]


def fill_gaps(df: pandas.DataFrame) -> pandas.DataFrame:
    set_all_keys = dict()
    array_all_keys = dict()
    for col in df.columns:
        set_all_keys[col] = set()
        array_all_keys[col] = []

    for col in df.columns:
        for elem in df[col].dropna():
            set_all_keys[col].add(elem)
            array_all_keys[col].append(elem)

    # power for all column
    for k, v in set_all_keys.items():
        set_all_keys[k] = len(v)

    res_for_gaps = dict()
    all_value = len(df.index)
    for col in df.columns:
        none_value = df[col].isna().sum()
        if col != "G_total (кг/с)" and col != "КГФ (г/м3)":
            res_for_gaps[col] = (none_value / all_value) * 100

    for col in df.columns:
        for i in range(0, len(df.index)):
            if col != "G_total (кг/с)" and col != "КГФ (г/м3)" and res_for_gaps[col] < 30:
                if set_all_keys[col] > 15:
                    if df.at[i, col] is np.nan or df.at[i, col] is None:
                        df.at[i, col] = round(numpy.mean(array_all_keys[col]), 1)
                else:
                    if df.at[i, col] is np.nan or df.at[i, col] is None:
                        df.at[i, col] = round(my_mode(array_all_keys[col]), 1)
    return df


def build_all_histogram(df: pandas.DataFrame):
    for col in df.columns:
        sns_plot = seaborn.histplot(df[col], kde=True, stat="density")
        sns_plot.get_figure()
        plt.show()


def remove_emissions(df: pandas.DataFrame) -> pandas.DataFrame:
    set_index = set()
    for i in range(0, len(df.index)):
        for col in df.columns:
            if col == "Осредненные Рлин (бара)":
                if df.at[i, col] is not None and df.at[i, col] < 40:
                    set_index.add(i)
            if col == "На конец режима Рлин (бар)":
                if df.at[i, col] is not None and df.at[i, col] < 60:
                    set_index.add(i)
            if col == "Линейные условия Дебит газа (м3/сут)":
                if df.at[i, col] is not None and df.at[i, col] > 10000:
                    set_index.add(i)
            if col == "Удельная плотность газа (б/р)":
                if df.at[i, col] is not None and df.at[i, col] < 0.64:
                    set_index.add(i)
            if col == "Тна шлейфе (С)":
                if df.at[i, col] is not None and df.at[i, col] > 80:
                    set_index.add(i)
            if col == "Ro_c (кг/м3)":
                if df.at[i, col] is not None and df.at[i, col] < 200:
                    set_index.add(i)
    for i in set_index:
        df = df.drop(i)
    df.index = np.arange(len(df))
    return df


def minmax_in_column(df: pandas.DataFrame, column: '') -> Tuple[Any, Any]:
    min_value = df[column].min()
    max_value = df[column].max()
    return min_value, max_value


def split_column_according_sturges_rule(df: pandas.DataFrame, col: '') -> []:
    n = 1 + round(np.log2(len(df.index) - df["G_total (кг/с)"].isna().sum()))
    min, max = minmax_in_column(df, col)
    a = (max - min) / n
    res = []
    for i in range(1, n + 1):
        res.append([min + a * (i - 1), min + a * i, 0])
    for i in range(0, len(df.index)):
        for j in range(0, n):
            if df.at[i, col] is not None and res[j][0] <= df.at[i, col] <= res[j][1]:
                res[j][2] += 1
                break
    return res


def entropy(freq: []) -> float:
    N = sum(freq)
    if N == 0:
        return 0
    final_sum = 0
    for i in range(0, len(freq)):
        pi = freq[i] / N
        if pi != 0:
            final_sum -= pi * np.log2(pi)
    return final_sum


def conditional_entropy(df: pandas.DataFrame, target: '', feature: '', target_splitting: [[]]):
    res = split_column_according_sturges_rule(df, feature)
    N = sum([res[j][2] for j in range(0, len(res))])
    final_sum = 0
    for j in range(0, len(res)):
        freq = [0 for _ in range(len(res))]
        for i in range(0, len(df.index)):
            if df.at[i, feature] is not None and res[j][0] <= df.at[i, feature] <= res[j][1]:
                for k in range(0, len(target_splitting)):
                    if df.at[i, target] is not None and target_splitting[k][0] <= df.at[i, target] <= \
                            target_splitting[k][1]:
                        freq[k] += 1
        final_sum += entropy(freq) * (res[j][2] / N)
    return final_sum


def split_info(df: pandas.DataFrame, feature: ''):
    res = split_column_according_sturges_rule(df, feature)
    N = sum([res[j][2] for j in range(0, len(res))])
    final_sum = 0
    for j in range(0, len(res)):
        ni = res[j][2] / N
        if ni != 0:
            final_sum -= ni * np.log2(ni)
    return final_sum


def all_gain_ratio_for_target(df: pandas.DataFrame, target: '') -> {}:
    split_target = split_column_according_sturges_rule(df, target)
    freq = []
    for i in range(0, len(split_target)):
        freq.append(split_target[i][2])

    res_dict = dict()
    for temp_col in df.columns:
        if temp_col != "G_total (кг/с)" and temp_col != "КГФ (г/м3)":
            res_dict[temp_col] = \
                (entropy(freq) - conditional_entropy(df, target, temp_col, split_target)) / split_info(df, temp_col)
    return res_dict


def build_all_gain_ratios_for_target(df: pandas.DataFrame):
    gain_ratio_g_total = all_gain_ratio_for_target(df, 'G_total (кг/с)')
    gain_ratio_kgf = all_gain_ratio_for_target(df, 'КГФ (г/м3)')

    fig = px.bar(y=gain_ratio_g_total.keys(), x=gain_ratio_g_total.values(), title="G_total (кг/с) gain ratios")
    fig.update_layout(bargap=0)
    fig.show()

    fig = px.bar(y=gain_ratio_kgf.keys(), x=gain_ratio_kgf.values(), title="КГФ (г/м3) gain ratios", )
    fig.update_layout(bargap=0)
    fig.show()


def build_corr_matrix(df: pandas.DataFrame):
    df_corr = df.corr(numeric_only=False).round(decimals=2)

    fig = ff.create_annotated_heatmap(
        np.array(abs(df_corr)),
        x=list(df_corr.columns),
        y=list(df_corr.index),
        annotation_text=np.around(np.array(abs(df_corr)), decimals=2),
        hoverinfo='z',
        colorscale='Viridis'
    )
    fig.update_layout(width=1400, height=1000)
    fig.show()

    gain_ratio_g_total = all_gain_ratio_for_target(df, 'G_total (кг/с)')
    gain_ratio_kgf = all_gain_ratio_for_target(df, 'КГФ (г/м3)')
    for col1 in df_corr.columns:
        for col2 in df_corr.columns:
            sum = 0
            for i in df_corr.columns:
                sum += abs(df_corr.at[i, col1] - df_corr.at[i, col2])
            if sum < 2 and col1 != col2:
                print(col1, col2, sum)
                print("IGR G_total: ", gain_ratio_g_total[col1], gain_ratio_g_total[col2])
                print("IGR КГФ: ", gain_ratio_kgf[col1], gain_ratio_kgf[col2])
                print()


def main():
    results = pd.read_csv('ListExcel.csv', sep=";")

    results.drop('№', axis=1, inplace=True)
    results.drop('дд.мм.гггг', axis=1, inplace=True)
    results.drop('Pсб (атм)', axis=1, inplace=True)

    results = convert_all_values(results)

    # merge two КГФ
    for i in range(0, len(results.index)):
        results.at[i, "КГФ (г/м3)"] = results.at[i, "КГФ (т/тыс.м3)"] * 1000
    results.drop("КГФ (т/тыс.м3)", axis=1, inplace=True)

    results = remove_row_with_many_gaps(results)
    results = remove_column_with_many_gaps(results)
    results = fill_gaps(results)
    # build_all_histogram(results)
    results = remove_emissions(results)
    # build_all_gain_ratios_for_target(results)
    # build_corr_matrix(results)

    # remove due to low correlation
    results.drop('Глубина манометра (м)', axis=1, inplace=True)
    results.drop('Нэф (м)', axis=1, inplace=True)
    results.drop('Рпл. Тек (послед точка на КВД) (МПа)', axis=1, inplace=True)
    results.drop('Рпл. Тек (Расчет по КВД) (МПа)', axis=1, inplace=True)
    results.drop('Рпл. Тек (Карноухов) (МПа)', axis=1, inplace=True)
    results.drop('Pсб (бар)', axis=1, inplace=True)
    results.drop('Ro_c (кг/м3)', axis=1, inplace=True)
    results.drop('Удельная плотность газа (б/р)', axis=1, inplace=True)
    results.drop('Тзаб (С)', axis=1, inplace=True)

    # remove due to has same feature
    results.drop('На конец режима Руст (бар)', axis=1, inplace=True)
    results.drop('Осредненные Рзаб (бара)', axis=1, inplace=True)
    results.drop('На конец режима Рлин (бар)', axis=1, inplace=True)
    results.drop('Линейные условия Дебит воды (м3/сут)', axis=1, inplace=True)

    # remove due to low correlation and gain ratio
    results.drop('На конец режима Рзаб (бар)', axis=1, inplace=True)
    results.drop('Осредненные Рлин (бара)', axis=1, inplace=True)
    results.drop('Осредненные Руст (бара)', axis=1, inplace=True)

    # build_corr_matrix(results)
    results.to_csv('NewList.csv', index=False)


if __name__ == "__main__":
    main()
