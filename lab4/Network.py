import numpy as np
import Layer


class Network(object):
    def __init__(self):
        self.layers = []
        self.layer_count = 0
        self.loss_function = None
        self.loss_list = []

    def add_layer(self, layer: Layer):
        self.layers.append(layer)
        self.layer_count += 1

    def forward_prop(self, input_data):
        in_v = input_data
        for layer in self.layers:
            in_v = layer.forward(in_v)
        return in_v

    def get_predictions(self, input_data):
        predictions = []
        for item in input_data:
            predictions.append(self.forward_prop(item))
        return np.array(predictions)

    def train_step(self, input_item, expected_output_item, learning_rate: float):
        # forward propagation:
        output = self.forward_prop(input_item)
        loss_value = self.loss_function(output, expected_output_item)

        # backward propagation:
        der_cost_result = expected_output_item
        for layer_idx in range(self.layer_count - 1, -1, -1):
            cur_layer = self.layers[layer_idx]
            der_cost_result = cur_layer.backward(der_cost_result)

        # change net parameters:
        for layer in self.layers:
            layer.change_parameters(learning_rate)

        return loss_value

    def train(self, input_data, expected_output_data, learning_rate: float, epoch_count: int, loss):
        self.loss_function = loss
        for epoch in range(epoch_count):
            epoch_losses = []
            for item_idx in range(len(input_data)):
                loss_value = self.train_step(input_data[item_idx], expected_output_data[item_idx], learning_rate)
                epoch_losses.append(loss_value)
            cost = np.average(np.array(epoch_losses))
            print("loss = " + str(cost))
            self.loss_list.append(cost)
