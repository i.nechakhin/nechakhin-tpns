import numpy as np
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.express as px

from metrics import false_positive_rate, true_positive_rate
from tools import get_predictions


def get_ROC_points(prob_predictions, actual_predictions, target_class, get_result_predictions):
    thresholds = np.linspace(0, 1, 1000)
    thresholds = np.flip(thresholds, axis=0)
    x_points = []
    y_points = []

    for threshold in thresholds:
        result_predictions = get_result_predictions(prob_predictions, target_class, threshold)
        result_actual_predictions = get_result_predictions(actual_predictions, target_class, threshold)
        fpr = false_positive_rate(result_predictions, result_actual_predictions, target_class)
        tpr = true_positive_rate(result_predictions, result_actual_predictions, target_class)
        x_points.append(fpr)
        y_points.append(tpr)

    return x_points, y_points


def plot_ROC(predictions, actual_predictions):
    plots = make_subplots(rows=5, cols=2, subplot_titles=('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'))

    for i in range(10):
        points = get_ROC_points(predictions, actual_predictions, i, get_predictions)
        x_data = np.array(points[0])
        y_data = np.array(points[1])

        col = (i % 2) + 1
        row = (i - col + 1) // 2 + 1
        plots.add_trace(go.Scatter(x=x_data, y=y_data, mode="markers"), row=row, col=col)

    plots.update_layout(height=2200, width=1000, title_text="ROC-lines")
    plots.show()


def plot_loss(loss_list):
    x = np.arange(1, len(loss_list) + 1)
    y = np.array(loss_list)
    fig = px.line(x=x, y=y, labels={'x': 'epoch', 'y': 'loss'})
    fig.show()
