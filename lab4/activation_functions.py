import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def th(x):
    return np.tanh(x)


def ReLU(x):
    result = np.zeros((len(x),))

    for i in range(len(x)):
        if x[i] >= 0:
            result[i] = x[i]
        else:
            result[i] = 0

    return result


def sigmoid_der(x):
    s = sigmoid(x)
    return s * (1 - s)


def th_der(x):
    return 1 - th(x) ** 2


def ReLU_der(x):
    if np.isscalar(x):
        if x >= 0:
            return 1
        else:
            return 0
    else:
        x[x >= 0] = 1
        x[x < 0] = 0
    return x


def get_der(ident_func):
    if ident_func == sigmoid:
        return sigmoid_der
    elif ident_func == th:
        return th_der
    elif ident_func == ReLU:
        return ReLU_der
    else:
        assert False
