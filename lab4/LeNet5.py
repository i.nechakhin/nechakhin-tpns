import numpy as np
import pandas as pd

from Layer import CNNlayer, MaxPoolingLayer, ReformatLayer, DenseLayer, SoftmaxLayer
from Network import Network
from activation_functions import ReLU, sigmoid
from metrics import log_loss, accuracy, recall, precision, f1_measure
from plot import plot_ROC, plot_loss
from tools import get_predictions


def refactor_out_data(out_data):
    refactored = np.zeros((len(out_data), 10))
    for i in range(len(out_data)):
        refactored[i][out_data[i]] = 1
    return refactored


def full_train(target_name, train_df: pd.DataFrame, test_df: pd.DataFrame):
    all_attributes = train_df.columns.to_list()
    output_attributes = [target_name]
    input_attributes = np.setdiff1d(np.array(all_attributes), output_attributes)

    train_in_data = train_df[input_attributes].to_numpy()
    train_out_data = train_df[output_attributes].to_numpy()
    test_in_data = test_df[input_attributes].to_numpy()
    test_out_data = test_df[output_attributes].to_numpy()

    train_in_data = train_in_data.reshape(60000, 1, 28, 28)
    test_in_data = test_in_data.reshape(10000, 1, 28, 28)
    train_out_data = refactor_out_data(train_out_data)

    train_in_data = train_in_data / 255
    test_in_data = test_in_data / 255

    cnnLayer = CNNlayer((1, 28, 28), ReLU, 6, (5, 5), 1)
    maxPolingLayer = MaxPoolingLayer((4, 4), cnnLayer.output_shape)
    reformatLayer = ReformatLayer(maxPolingLayer.output_shape)
    denseLayer = DenseLayer(sigmoid, 10, reformatLayer.output_shape[0])
    softmaxLayer = SoftmaxLayer(10)

    net = Network()
    net.add_layer(cnnLayer)
    net.add_layer(maxPolingLayer)
    net.add_layer(reformatLayer)
    net.add_layer(denseLayer)
    net.add_layer(softmaxLayer)

    '''
    cnnLayer1 = CNNlayer((1, 28, 28), ReLU, 6, (5, 5), 1)
    maxPooling1 = MaxPoolingLayer((2, 2), cnnLayer1.output_shape)
    cnnLayer2 = CNNlayer(maxPooling1.output_shape, ReLU, 16, (3, 3), 1)
    maxPooling2 = MaxPoolingLayer((2, 2), cnnLayer2.output_shape)
    reformatLayer = ReformatLayer(maxPooling2.output_shape)
    dense1 = DenseLayer(ReLU, 50, reformatLayer.output_shape[0])
    dense2 = DenseLayer(sigmoid, 10, dense1.output_shape[0])
    softmaxLayer = SoftmaxLayer(10)

    net = Network()
    net.add_layer(cnnLayer1)
    net.add_layer(maxPooling1)
    net.add_layer(cnnLayer2)
    net.add_layer(maxPooling2)
    net.add_layer(reformatLayer)
    net.add_layer(dense1)
    net.add_layer(dense2)
    net.add_layer(softmaxLayer)
    '''

    net.train(train_in_data[:1000], train_out_data[:1000], learning_rate=0.001, epoch_count=5, loss=log_loss)

    plot_loss(net.loss_list)

    test_predictions = np.zeros((len(test_in_data[:1000]), 10))
    for i in range(len(test_in_data[:1000])):
        test_predictions[i] = net.forward_prop(test_in_data[i])

    plot_ROC(test_predictions, refactor_out_data(test_out_data[:1000]))
    for thresholds in range(1, 10):
        print("----- for thresholds = " + str(thresholds / 10) + " -----")
        for i in range(10):
            test_pred = get_predictions(test_predictions, i, thresholds / 10)
            test_out = test_out_data[:1000].reshape(1000)
            print("-----for " + str(i) + " number-----")
            print("Accuracy - " + str(accuracy(test_pred, test_out, i)))
            test_recall = recall(test_pred, test_out, i)
            print("Recall - " + str(test_recall))
            test_precision = precision(test_pred, test_out, i)
            print("Precision - " + str(test_precision))
            print("F1-measure - " + str(f1_measure(test_precision, test_recall)))


train_data_set = pd.read_csv('data_sets/mnist_train.csv')
test_data_set = pd.read_csv('data_sets/mnist_test.csv')
full_train('label', train_data_set, test_data_set)
