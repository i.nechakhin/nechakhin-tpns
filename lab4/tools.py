import numpy as np


def get_sum_x_from_maps(maps, y, x):
    result = 0
    for map in maps:
        result += map[y][x]
    return result


def get_bound_point(image_shape, core_shape, point_x, point_y):
    im_width = image_shape[1]
    im_height = image_shape[0]

    core_width = core_shape[1]
    core_height = core_shape[0]

    left_up_y = max(point_y - core_height + 1, 0)
    left_up_x = max(point_x - core_width + 1, 0)

    right_down_x = min(point_x, im_width - core_width)
    right_down_y = min(point_y, im_height - core_height)

    return (left_up_y, left_up_x), (right_down_y, right_down_x)


def get_size(tuple: tuple):
    result = 1
    for i in range(len(tuple)):
        result *= tuple[i]
    return result


def get_prediction(prob_prediction, target_class, threshold):
    if prob_prediction[target_class] >= threshold:
        return int(target_class)
    else:
        return -1


def get_predictions(prob_predictions, target_class, threshold):
    pred_count = len(prob_predictions)
    result_predictions = np.zeros((pred_count,)) - 1
    for i in range(pred_count):
        result_predictions[i] = get_prediction(prob_predictions[i], target_class, threshold)
    return result_predictions