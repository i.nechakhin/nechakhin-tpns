from abc import ABC, abstractmethod

import numpy as np

from activation_functions import get_der
from tools import get_sum_x_from_maps, get_bound_point, get_size


class Layer(ABC):
    @abstractmethod
    def forward(self, input):
        pass

    @abstractmethod
    def backward(self, arg):
        pass

    @abstractmethod
    def change_parameters(self, learning_rate):
        pass


class CNNlayer(Layer):
    def __init__(self, in_shape, activation_f, cores_count, cores_shape, stride):
        self.input = None
        self.act_function = activation_f
        self.stride = stride
        self.output_shape = (
            cores_count, (in_shape[1] - cores_shape[0]) // stride + 1, (in_shape[2] - cores_shape[1]) // stride + 1)
        self.z_array = np.zeros(self.output_shape)
        self.output = np.zeros(self.output_shape)

        self.cores = np.random.rand(cores_count, cores_shape[0], cores_shape[1]) * 2 - 1
        self.biases = np.random.rand(cores_count) * 2 - 1

        self.cores_weights_derivatives = np.zeros(self.cores.shape)
        self.biases_derivatives = np.zeros(self.biases.shape)
        self.x_derivatives_array = np.zeros(in_shape)

    def accept_filter(self, core_idx, image):
        output_map = np.zeros((self.output_shape[1], self.output_shape[2]))
        filter_core = self.cores[core_idx]
        for i in range(self.output_shape[1]):
            for j in range(self.output_shape[2]):
                angle_x = j * self.stride
                angle_y = i * self.stride
                z = 0
                for core_y in range(filter_core.shape[0]):
                    for core_x in range(filter_core.shape[1]):
                        z += filter_core[core_y][core_x] * image[angle_y + core_y][angle_x + core_x]
                output_map[i][j] = z
        return output_map

    def forward(self, input_map):
        self.input = input_map
        map_count = input_map.shape[0]
        for core_idx in range(len(self.cores)):
            result_map = np.zeros((self.output_shape[1], self.output_shape[2]))
            for map_idx in range(map_count):
                result_map += self.accept_filter(core_idx, input_map[map_idx])
            self.z_array[core_idx] = result_map + self.biases[core_idx]
            self.output[core_idx] = np.apply_along_axis(self.act_function, 0, self.z_array[core_idx])
        return self.output

    def get_der_cost_weight(self, core_x, core_y, core_idx, der_cost_result, input_act_maps):
        result_der = 0
        for y in range(self.output_shape[1]):
            for x in range(self.output_shape[2]):
                result_der += der_cost_result[core_idx][y][x] * \
                              get_der(self.act_function)(self.z_array[core_idx][y][x]) * \
                              get_sum_x_from_maps(input_act_maps, y * self.stride + core_y, x * self.stride + core_x)
        return result_der

    def der_cost_cores(self, der_cost_result):  # calc dL/dWij for each core
        for core_idx in range(len(self.cores)):
            core = self.cores[core_idx]
            for core_y in range(core.shape[0]):
                for core_x in range(core.shape[1]):
                    self.cores_weights_derivatives[core_idx][core_y][core_x] = \
                        self.get_der_cost_weight(core_x, core_y, core_idx, der_cost_result, self.input)

    def der_cost_biases(self, der_cost_result):
        for biases_idx in range(len(self.biases)):
            result_der = 0
            for y in range(self.output_shape[1]):
                for x in range(self.output_shape[2]):
                    result_der += der_cost_result[biases_idx][y][x] * \
                                  get_der(self.act_function)(self.z_array[biases_idx][y][x])
            self.biases_derivatives[biases_idx] = result_der

    def get_der_cost_x(self, core_idx, input_act_map, y, x, der_cost_result):
        left_up_point, right_down_point = get_bound_point(input_act_map.shape, self.cores[core_idx].shape, x, y)
        result_der = 0
        core = self.cores[core_idx]
        for core_y_on_map in np.arange(left_up_point[0], right_down_point[0]):
            for core_x_on_map in np.arange(left_up_point[1], right_down_point[1]):
                result_der += der_cost_result[core_idx][core_y_on_map][core_x_on_map] * \
                              core[y - core_y_on_map][x - core_x_on_map] * \
                              get_der(self.act_function)(self.z_array[core_idx][core_y_on_map][core_x_on_map])
        return result_der

    def der_cost_input(self, der_cost_result):
        for input_map_idx in range(self.input.shape[0]):
            for y in range(self.input.shape[1]):
                for x in range(self.input.shape[2]):
                    result_point_derivative = 0
                    for core_idx in range(len(self.cores)):
                        activation_map = self.input[input_map_idx]
                        result_point_derivative += self.get_der_cost_x(core_idx, activation_map, y, x, der_cost_result)
                    self.x_derivatives_array[input_map_idx][y][x] = result_point_derivative
        return self.x_derivatives_array

    def backward(self, arg):
        self.der_cost_cores(arg)
        self.der_cost_biases(arg)
        return self.der_cost_input(arg)

    def change_parameters(self, learning_rate):
        l = lambda x: learning_rate * x
        self.cores -= np.apply_along_axis(func1d=l, axis=0, arr=self.cores_weights_derivatives)
        self.biases -= np.apply_along_axis(func1d=l, axis=0, arr=self.biases_derivatives)


class MaxPoolingLayer(Layer):
    def __init__(self, core_shape, in_shape):
        self.input_shape = in_shape
        self.core_shape = core_shape
        self.x_derivatives_array = None
        self.output_shape = (in_shape[0], in_shape[1] // core_shape[0], in_shape[2] // core_shape[1])
        self.input_maximums_positions = np.zeros((self.output_shape[0], self.output_shape[1], self.output_shape[2], 2))
        self.output = np.zeros(self.output_shape)

    def accept_pooling(self, image, map_idx):
        output = np.zeros((self.output_shape[1], self.output_shape[2]))
        for y in range(self.output_shape[1]):
            for x in range(self.output_shape[2]):
                angle_x = x * self.core_shape[1]
                angle_y = y * self.core_shape[0]
                cur_max_pos = [angle_y, angle_x]
                cur_max_value = image[angle_y][angle_x]
                for core_y in range(self.core_shape[0]):
                    for core_x in range(self.core_shape[1]):
                        cur_pixel_value = image[angle_y + core_y][angle_x + core_x]
                        if cur_max_value < cur_pixel_value:
                            cur_max_value = cur_pixel_value
                            cur_max_pos = [angle_y + core_y, angle_x + core_x]
                self.input_maximums_positions[map_idx][y][x] = cur_max_pos
                output[y][x] = cur_max_value
        return output

    def forward(self, input):
        map_count = input.shape[0]
        for map_idx in range(map_count):
            self.output[map_idx] = self.accept_pooling(input[map_idx], map_idx)
        return self.output

    def der_cost_input(self, der_cost_result):
        self.x_derivatives_array = np.zeros(self.input_shape)
        for output_map_idx in range(self.output.shape[0]):
            for y in range(self.output.shape[1]):
                for x in range(self.output.shape[2]):
                    point = self.input_maximums_positions[output_map_idx][y][x]
                    self.x_derivatives_array[output_map_idx][int(point[0])][int(point[1])] = \
                        der_cost_result[output_map_idx][y][x]
        return self.x_derivatives_array

    def backward(self, arg):
        return self.der_cost_input(arg)

    def change_parameters(self, learning_rate):
        pass


class DenseLayer(Layer):
    def __init__(self, activation_f, neuron_count, prev_layer_neuron_count):
        self.output_shape = (neuron_count,)
        self.input_shape = (prev_layer_neuron_count,)
        self.neuron_count = neuron_count
        self.activations = None
        self.input = None
        self.act_function = activation_f
        self.weights_matrix = np.random.rand(prev_layer_neuron_count, neuron_count) * 2 - 1
        self.biases = np.random.rand(neuron_count, 1)
        self.weights_der_array = np.zeros(self.weights_matrix.shape)
        self.biases_der_array = np.zeros(self.biases.shape)
        self.x_derivatives_array = np.zeros(self.input_shape)

    def forward(self, input_vector):
        self.input = input_vector
        self.activations = np.matmul(self.weights_matrix.transpose(), np.array([self.input]).transpose()) + self.biases
        return np.apply_along_axis(func1d=self.act_function, axis=0, arr=self.activations).reshape(self.output_shape)

    def der_cost_weights(self, der_cost_result):
        for i in range(self.weights_matrix.shape[0]):
            for j in range(self.weights_matrix.shape[1]):
                self.weights_der_array[i][j] = der_cost_result[j] * get_der(self.act_function)(self.activations[j][0]) * \
                                               self.input[i]

    def der_cost_biases(self, der_cost_result):
        for b_idx in range(len(self.biases)):
            self.biases_der_array[b_idx][0] = der_cost_result[b_idx] * \
                                              get_der(self.act_function)(self.activations[b_idx][0])

    def der_cost_input(self, der_cost_result):
        prev_layer_neuron_count = self.input_shape[0]
        cur_layer_neuron_count = self.neuron_count
        for prev_layer_neuron_idx in range(prev_layer_neuron_count):
            result_der = 0
            for cur_layer_neuron_idx in range(cur_layer_neuron_count):
                result_der += der_cost_result[cur_layer_neuron_idx] * \
                              get_der(self.act_function)(self.activations[cur_layer_neuron_idx]) \
                              * self.weights_matrix[prev_layer_neuron_idx][cur_layer_neuron_idx]
            self.x_derivatives_array[prev_layer_neuron_idx] = result_der
        return self.x_derivatives_array

    def backward(self, arg):
        self.der_cost_weights(arg)
        self.der_cost_biases(arg)
        return self.der_cost_input(arg)

    def change_parameters(self, learning_rate):
        l = lambda x: learning_rate * x
        self.weights_matrix -= np.apply_along_axis(func1d=l, axis=0, arr=self.weights_der_array)
        self.biases -= np.apply_along_axis(func1d=l, axis=0, arr=self.biases_der_array)


class ReformatLayer(Layer):
    def __init__(self, input_shape):
        self.output_shape = (get_size(input_shape),)
        self.x_derivatives_array = None
        self.input_shape = input_shape

    def forward(self, input):
        result = input[0].flatten()
        for i in range(input.shape[0] - 1):
            map_idx = i + 1
            cur_map = input[map_idx]
            result = np.concatenate((result, cur_map), axis=None)
        return result

    def backward(self, arg):
        self.x_derivatives_array = arg.reshape(self.input_shape)
        return self.x_derivatives_array

    def change_parameters(self, learning_rate):
        pass


class SoftmaxLayer(Layer):
    def __init__(self, neuron_count):
        self.x_derivatives_array = np.zeros((neuron_count,))
        self.output = None

    def forward(self, input):
        exp_arr = np.exp(input)
        self.output = exp_arr / np.sum(exp_arr)
        return self.output

    def backward(self, arg):
        self.x_derivatives_array = self.output - arg
        return self.x_derivatives_array

    def change_parameters(self, learning_rate):
        pass
