import numpy as np


def log_loss(predicted_vector, dst_vector) -> float:
    return -np.sum(np.log(predicted_vector).transpose() * dst_vector)


def false_positive(result_predictions, actual_predictions, target_class) -> int:
    result = 0
    for i in range(len(result_predictions)):
        prediction = result_predictions[i]
        actual = actual_predictions[i]
        if actual != target_class and prediction == target_class:
            result += 1
    return result


def false_negative(result_predictions, actual_predictions, target_class) -> int:
    result = 0
    for i in range(len(result_predictions)):
        prediction = result_predictions[i]
        actual = actual_predictions[i]
        if actual == target_class and prediction != target_class:
            result += 1
    return result


def true_negative(result_predictions, actual_predictions, target_class) -> int:
    result = 0
    for i in range(len(result_predictions)):
        prediction = result_predictions[i]
        actual = actual_predictions[i]
        if actual != target_class and prediction != target_class:
            result += 1
    return result


def true_positive(result_predictions, actual_predictions, target_class) -> int:
    result = 0
    for i in range(len(result_predictions)):
        prediction = result_predictions[i]
        actual = actual_predictions[i]
        if actual == target_class and prediction == target_class:
            result += 1
    return result


def false_positive_rate(result_predictions, actual_predictions, target_class) -> float:
    fp = false_positive(result_predictions, actual_predictions, target_class)
    tn = true_negative(result_predictions, actual_predictions, target_class)
    if (fp + tn) == 0:
        return 0
    else:
        return fp / (fp + tn)


def true_positive_rate(result_predictions, actual_predictions, target_class) -> float:
    tp = true_positive(result_predictions, actual_predictions, target_class)
    fn = false_negative(result_predictions, actual_predictions, target_class)
    if (tp + fn) == 0:
        return 0
    else:
        return tp / (tp + fn)


def recall(result_predictions, actual_predictions, target_class):
    tp = true_positive(result_predictions, actual_predictions, target_class)
    fn = false_negative(result_predictions, actual_predictions, target_class)
    if (tp + fn) == 0:
        return 0
    else:
        return tp / (tp + fn)


def accuracy(result_predictions, actual_predictions, target_class):
    tp = true_positive(result_predictions, actual_predictions, target_class)
    tn = true_negative(result_predictions, actual_predictions, target_class)
    fp = false_positive(result_predictions, actual_predictions, target_class)
    fn = false_negative(result_predictions, actual_predictions, target_class)
    if (tp + tn + fp + fn) == 0:
        return 0
    else:
        return (tp + tn) / (tp + tn + fp + fn)


def precision(result_predictions, actual_predictions, target_class):
    tp = true_positive(result_predictions, actual_predictions, target_class)
    fp = false_positive(result_predictions, actual_predictions, target_class)
    if (tp + fp) == 0:
        return 0
    else:
        return tp / (tp + fp)


def f1_measure(precision, recall):
    if (precision + recall) == 0:
        return 0
    else:
        return 2 * precision * recall / (precision + recall)
